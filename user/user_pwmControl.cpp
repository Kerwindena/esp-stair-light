#include "user_pwmControl.h"
#include "missingincludes.h"

#include <math.h>

extern "C"
{
#include "pwm.h"
}

// Begin User Configuration

#define PWM_PIN_12 true
#define PWM_PIN_13 true
#define PWM_PIN_15 true

#define PWM_FREQUENCY 1000
#define PWM_STEPS     1024

#define PWM_AUTO_FINALIZE false

// End User Configuration

#define PWM_MAXVALUE PWM_FREQUENCY*1000/45
#define DUTY_TO_VALUE(x) (uint32)(((exp(1.0*x/(PWM_STEPS-1)*exp(1)))-1)/(exp(exp(1))-1)*PWM_MAXVALUE)

#if PWM_AUTO_FINALIZE
#define PWM_AUTO_FINALIZE_CMD() pwm_start();
#else
#define PWM_AUTO_FINALIZE_CMD() 
#endif

#if PWM_PIN_12
  #define PWM_CHANNEL_0 pwmControl::pwmPin_12
  #define PWM_PIN_12_CHANNEL 0
  #if PWM_PIN_13
    #define PWM_CHANNEL_1 pwmControl::pwmPin_13
    #define PWM_PIN_13_CHANNEL 1
    #if PWM_PIN_15
      #define PWM_CHANNEL_2 pwmControl::pwmPin_15
      #define PWM_PIN_15_CHANNEL 2
    #endif
  #else
    #if PWM_PIN_15
      #define PWM_CHANNEL_1 pwmControl::pwmPin_15
      #define PWM_PIN_15_CHANNEL 1
    #endif
  #endif
#else
  #if PWM_PIN_13
    #define PWM_CHANNEL_0 pwmControl::pwmPin_13
    #define PWM_PIN_13_CHANNEL 0
    #if PWM_PIN_15
      #define PWM_CHANNEL_1 pwmControl::pwmPin_15
      #define PWM_PIN_15_CHANNEL 1
    #endif
  #else
    #if PWM_PIN_15
      #define PWM_CHANNEL_0 pwmControl::pwmPin_15
      #define PWM_PIN_15_CHANNEL 0
    #endif
  #endif
#endif

#ifdef PWM_CHANNEL_0
#define PWM_CHANNELS 1
#endif
#ifdef PWM_CHANNEL_1
#undef PWM_CHANNELS
#define PWM_CHANNELS 2
#endif
#ifdef PWM_CHANNEL_2
#undef PWM_CHANNELS
#define PWM_CHANNELS 3
#endif
#ifndef PWM_CHANNELS
#error No PWM-channels are enabled!
#endif

#if PWM_PIN_12
uint16 pwmControl::pwmPin_12::duty = 0;
const uint8 pwmControl::pwmPin_12::index = PWM_PIN_12_CHANNEL;
#endif
#if PWM_PIN_13
uint16 pwmControl::pwmPin_13::duty = 0;
const uint8 pwmControl::pwmPin_13::index = PWM_PIN_13_CHANNEL;
#endif
#if PWM_PIN_15
uint16 pwmControl::pwmPin_15::duty = 0;
const uint8 pwmControl::pwmPin_15::index = PWM_PIN_15_CHANNEL;
#endif

void pwmControl::initialize()
{
	
	uint32 duty[3] = {0,0,0};
	
	uint32 io_info[PWM_CHANNELS][3] = {
#ifdef PWM_CHANNEL_0
		{PWM_CHANNEL_0::io_info[0],PWM_CHANNEL_0::io_info[1],PWM_CHANNEL_0::io_info[2]},
#endif
#ifdef PWM_CHANNEL_1
		{PWM_CHANNEL_1::io_info[0],PWM_CHANNEL_1::io_info[1],PWM_CHANNEL_1::io_info[2]},
#endif
#ifdef PWM_CHANNEL_2
		{PWM_CHANNEL_2::io_info[0],PWM_CHANNEL_2::io_info[1],PWM_CHANNEL_2::io_info[2]},
#endif
	};
	
	pwm_init(PWM_FREQUENCY, duty, PWM_CHANNELS, io_info);
}

void pwmControl::finalize()
{
	pwm_start();
}

template<class pwmPin>
void pwmControl::setDuty(uint16 duty)
{
	pwmPin::duty = duty;
	pwm_set_duty(DUTY_TO_VALUE(duty), pwmPin::index);
	PWM_AUTO_FINALIZE_CMD();
}

template<class pwmPin>
uint8 pwmControl::getDuty()
{
	return pwm_get_duty(pwmPin::index);
}

void noncallable_pwmControl()
{
	//pseudo call every generic function to be called externally two times,
	//first time to generate it, second time to make it into a function not
	//some inline-code here. Might be possible in better way.
	#if PWM_PIN_12
	pwmControl::setDuty<struct pwmControl::pwmPin_12>(0);
	pwmControl::setDuty<struct pwmControl::pwmPin_12>(0);
	pwmControl::getDuty<struct pwmControl::pwmPin_12>();
	pwmControl::getDuty<struct pwmControl::pwmPin_12>();
	#endif
	#if PWM_PIN_13
	pwmControl::setDuty<struct pwmControl::pwmPin_13>(0);
	pwmControl::setDuty<struct pwmControl::pwmPin_13>(0);
	pwmControl::getDuty<struct pwmControl::pwmPin_13>();
	pwmControl::getDuty<struct pwmControl::pwmPin_13>();
	#endif
	#if PWM_PIN_15
	pwmControl::setDuty<struct pwmControl::pwmPin_15>(0);
	pwmControl::setDuty<struct pwmControl::pwmPin_15>(0);
	pwmControl::getDuty<struct pwmControl::pwmPin_15>();
	pwmControl::getDuty<struct pwmControl::pwmPin_15>();
	#endif
}
