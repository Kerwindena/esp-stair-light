#include "user_lightControl.h"
#include "user_pwmControl.h"

extern "C"
{
#include <mem.h>
#include <user_interface.h>
}

struct lightControl::commandq *lightControl::light0::commandq = nullptr;
struct lightControl::commandq *lightControl::light1::commandq = nullptr;
struct lightControl::commandq *lightControl::light2::commandq = nullptr;

uint32 lightControl::light0::startTime = 0;
uint32 lightControl::light1::startTime = 0;
uint32 lightControl::light2::startTime = 0;

template<class pwm, class light>
bool lightControl::update(uint16 duty)
{
	bool allowSleep = (duty == 0);
	uint32 value = duty;
	//1. Get Command, fail -> set duty
	if(light::commandq != nullptr)
		{
			allowSleep = false;
			//2. Get the time
			uint32 time = system_get_time() / 1000;
			if(light::startTime == 0)
				{
					if(time == 0) // Just in case
						time = 1;
					light::startTime = time;
				}
			time -= light::startTime;
			//3.
			//Case fade in
			//Case Graph
			//Case fade out
			//Case regression
			switch(light::commandq->command)
				{
				case FADE_IN:
				case FADE_OUT:
				case GRAPH:
					duty = drawGraph(light::commandq, time);
					if(light::commandq->command == FADE_IN)
						{
							duty = duty * time / light::commandq->length;
							value = value * (light::commandq->length - time) / light::commandq->length;
							duty += value;
						}
					if(light::commandq->command == FADE_OUT)
						{
							duty = duty * (light::commandq->length - time) / light::commandq->length;
							value = value * time / light::commandq->length;
							duty += value;
						}
					break;
				}
			if(time >= light::commandq->length)
				{
					struct commandq *item = light::commandq;
					light::commandq = item->next;
					os_free(item);
					light::startTime = 0;
				}
		}
	//4. Set PWM
	os_printf("%d\n", duty);
	pwmControl::setDuty<pwm>(duty);
	return allowSleep;
}

bool lightControl::update(uint16 duty)
{
	bool allowSleep = true;
	
	allowSleep &= update<struct pwmControl::pwmPin_12, struct light0>(duty);
	allowSleep &= update<struct pwmControl::pwmPin_13, struct light1>(duty);
	allowSleep &= update<struct pwmControl::pwmPin_15, struct light2>(duty);

	pwmControl::finalize();

	return allowSleep;
}

uint16 lightControl::drawGraph(struct commandq *command, uint32 x)
{
	double y = command->d;
	y = y * x + command->c;
	y = y * x + command->b;
	y = y * x + command->a;

	if(y <= 0)
		return 0;
	if(y >= 1024)
		return 1024;
	return (uint16)(y + .5); // round to nearest integer
}

template<class light>
void lightControl::addCommand(double a, double b, double c, double d, uint32 length, enum command command)
{
	struct commandq *item = (struct commandq*)os_malloc(sizeof(struct commandq));
	
	//fill the stuct with its content
	item->next = nullptr;
	item->a = a;
	item->b = b;
	item->c = c;
	item->d = d;
	item->length = length;
	item->command = command;

	//Link the item to the end of the queue
	if(light::commandq == nullptr)
		light::commandq = item;
	else
		{
			struct commandq *view = light::commandq;
			while(view->next != nullptr)
				view = view->next;
			view->next = item;
		}
}

void lightControl::initialize()
{

	pwmControl::initialize();
	
}

void noncallable_lightControl()
{
	//pseudo call every generic function to be called externally two times,
	//first time to generate it, second time to make it into a function not
	//some inline-code here. Might be possible in better way.
	lightControl::addCommand<struct lightControl::light0>(0, 0, 0, 0, 0, lightControl::REGRESSION);
	lightControl::addCommand<struct lightControl::light0>(0, 0, 0, 0, 0, lightControl::REGRESSION);
	lightControl::addCommand<struct lightControl::light1>(0, 0, 0, 0, 0, lightControl::REGRESSION);
	lightControl::addCommand<struct lightControl::light1>(0, 0, 0, 0, 0, lightControl::REGRESSION);
	lightControl::addCommand<struct lightControl::light2>(0, 0, 0, 0, 0, lightControl::REGRESSION);
	lightControl::addCommand<struct lightControl::light2>(0, 0, 0, 0, 0, lightControl::REGRESSION);
}
