#ifndef __ADC_H
#define __ADC_H

#include "missingincludes.h"

namespace adc
{
	static double value = 0;
	void feed();
	double getSmoothed();
};

#endif
