#include "user_adc.h"

extern "C"
{
#include <user_interface.h>
}

#define ALPHA 16384

void adc::feed()
{
	static uint32 alpha = 1;
	value *= alpha - 1;
	value += system_adc_read();
	value /= alpha;
	if(alpha < ALPHA)
		alpha++;
}

double adc::getSmoothed()
{
	return value;
}
