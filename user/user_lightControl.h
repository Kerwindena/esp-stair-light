#ifndef __LIGHTCONTROL_H
#define __LIGHTCONTROL_H

#include "missingincludes.h"

namespace lightControl
{
	enum command
	{
		REGRESSION,
		FADE_IN,
		GRAPH,
		FADE_OUT
	};
	
	struct commandq
	{
		struct commandq *next;
		enum command command;
		uint32 length;
		double a, b, c, d;
	};
	
	static struct light0
	{
		static struct commandq *commandq;
		static uint32 startTime;
	} light0;
	static struct light1
	{
		static struct commandq *commandq;
		static uint32 startTime;
	} light1;
	static struct light2
	{
		static struct commandq *commandq;
		static uint32 startTime;
	} light2;

	void initialize();

	bool update(uint16 duty);
	template<class pwm, class light>
		bool update(uint16 duty);

	template<class light>
		void addCommand(double a, double b, double c, double d, uint32 length, enum command command);

	uint16 drawGraph(struct commandq *command, uint32 x);

}

#endif
