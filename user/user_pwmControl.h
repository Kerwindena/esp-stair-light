#ifndef __PWMCONTROL_H
#define __PWMCONTROL_H

#include "missingincludes.h"

namespace pwmControl
{
	static const struct pwmPin_12
	{
		static uint16 duty;
		static const uint8 index;
		static constexpr uint32 io_info[3] = {PERIPHS_IO_MUX_MTDI_U,FUNC_GPIO12,12};
	} pwmPin_12;
	static const struct pwmPin_13
	{
		static uint16 duty;
		static const uint8 index;
		static constexpr uint32 io_info[3] = {PERIPHS_IO_MUX_MTCK_U,FUNC_GPIO13,13};
	} pwmPin_13;
	static const struct pwmPin_15
	{
		static uint16 duty;
		static const uint8 index;
		static constexpr uint32 io_info[3] = {PERIPHS_IO_MUX_MTDO_U,FUNC_GPIO15,15};
	} pwmPin_15;

	template<class pwmPin>
		void setDuty(uint16 duty);
	template<class pwmPin>
		uint8 getDuty();
	
	void initialize();
	void finalize();
};

#endif
