#include "missingincludes.h"

extern "C"
{
#include <osapi.h>
#include <gpio.h>
#include <user_config.h>
#include <user_interface.h>
}

#include "user_adc.h"
#include "user_lightControl.h"

#define user_procTaskPrio        0
#define user_procTaskQueueLen    1
os_event_t user_procTaskQueue[user_procTaskQueueLen];
static void user_procTask(os_event_t *events);

static volatile os_timer_t some_timer;

//Do nothing function
static void ICACHE_FLASH_ATTR
user_procTask(os_event_t *events)
{
	os_delay_us(10);
}

uint16 brightness_func(double d)
{
	if(d <= 350)
		return 0;
	else if(d >= 500)
		return 128;
	else if(d <= 450)
		{
			d -= 350;
			return (uint32)(d * d * (0.26880 - 0.0017920 * d));
		}
	else
		{
			d -= 450;
			return (uint32)(896 - d * d * (0.92160 - 0.012288 * d));
		}
}

void some_timerfunc(void *arg)
{
	bool allowSleep = false;
	
	adc::feed();
	
	//os_printf("ADC: %d  \tSmothed: %f\n", system_adc_read(), adc::getSmoothed());

	allowSleep = lightControl::update(brightness_func(adc::getSmoothed()));

	if(allowSleep)
		{
			//Disarm timer
			os_timer_disarm(&some_timer);
			
			//sleep for 5 minutes
			system_deep_sleep(1000 * 1000 * 60 * 5);
		};
	
}

void ICACHE_FLASH_ATTR
boot_up()
{
	
	typedef struct lightControl::light0 stair1;
	typedef struct lightControl::light1 stair2;
	typedef struct lightControl::light2 stair0;
	
	lightControl::addCommand<stair0>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair0>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair0>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair0>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair0>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair0>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair0>(0,0,0,0,400,lightControl::GRAPH);
	lightControl::addCommand<stair0>(0,0,0,0,1000,lightControl::FADE_OUT);
	
	lightControl::addCommand<stair1>(0,0,0,0,200,lightControl::GRAPH);
	lightControl::addCommand<stair1>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair1>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair1>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair1>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair1>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair1>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair1>(0,0,0,0,200,lightControl::GRAPH);
	lightControl::addCommand<stair1>(0,0,0,0,1000,lightControl::FADE_OUT);

	lightControl::addCommand<stair2>(0,0,0,0,400,lightControl::GRAPH);
	lightControl::addCommand<stair2>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair2>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair2>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair2>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair2>(0,0,0.012288,-1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair2>(1024,0,-0.012288,1.6384e-05,500,lightControl::GRAPH);
	lightControl::addCommand<stair2>(0,0,0,0,1000,lightControl::FADE_OUT);
	
}

//Init function
extern "C" void ICACHE_FLASH_ATTR
user_init()
{
	// Set UART on a 115200 BAUD-rate
	uart_div_modify(0, UART_CLK_FREQ / 115200);
	
	// Initialize the GPIO subsystem.
	gpio_init();
	
	lightControl::initialize();

	if (system_get_rst_info()->reason != REASON_DEEP_SLEEP_AWAKE)
		{
			boot_up();
		}
	
	//Disarm timer
	os_timer_disarm(&some_timer);
	
	//Setup timer
	os_timer_setfn(&some_timer, (os_timer_func_t *)some_timerfunc, NULL);
	
	//Arm the timer
	//&some_timer is the pointer
	//1000 is the fire time in ms
	//0 for once and 1 for repeating
	os_timer_arm(&some_timer, 10, 1);
  
	//Start os task
	system_os_task(user_procTask, user_procTaskPrio, user_procTaskQueue, user_procTaskQueueLen);
}
