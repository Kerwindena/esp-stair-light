#ifndef __MISSINGINCLUIDES_H
#define __MISSINGINCLUIDES_H

extern "C"
{

#include <ets_sys.h>
#include <os_type.h>


	// UART stuff

	void uart_div_modify(int no, unsigned int freq);

	int ets_uart_printf(const char *fmt, ...);
	int ets_printf(const char *fmt, ...);
	int ets_sprintf(char *str, const char *format, ...);

	int os_printf_plus(const char *fmt, ...);
#define os_printf os_printf_plus
	int os_snprintf(char *str, size_t size, const char *fmt, ...);

	void ets_install_putc1(void *routine);


	// Timer stuff
	void ets_timer_arm_new(volatile ETSTimer *a, int b, int c, int isMstimer);
	void ets_timer_disarm(volatile ETSTimer *a);
	void ets_timer_setfn(volatile ETSTimer *t, ETSTimerFunc *fn, void *parg);
	void ets_delay_us (int us);


	// String stuff

	int ets_str2macaddr(void *, void *);
	int ets_strcmp(const char *s1, const char *s2);
	char *ets_strcpy(char *dest, const char *src);
	size_t ets_strlen(const char *s);
	int ets_strncmp(const char *s1, const char *s2, int len);
	char *ets_strncpy(char *dest, const char *src, size_t n);
	char *ets_strstr(const char *haystack, const char *needle);


	// Memory stuff

	int ets_memcmp(const void *s1, const void *s2, size_t n);
	void *ets_memcpy(void *dest, const void *src, size_t n);
	void *ets_memset(void *s, int c, size_t n);

	void ets_bzero(void *s, size_t n);

	void *pvPortMalloc(size_t xWantedSize, const char *file, int line);
	void *pvPortCalloc(size_t xWantedSize, const char *file, int line);
	void *pvPortRealloc(void* ptr, size_t xWantedSize, const char *file, int line);
	void *pvPortZalloc(size_t, const char *file, int line);
	void vPortFree(void *ptr, const char *file, int line);
	
	void pvPortFree(void *ptr);
	void *vPortMalloc(size_t xWantedSize);


	// Miscellaneous

	int skip_atoi(const char **nptr);
	int atoi(const char *nptr);

	void ets_isr_attach(int intr, void *handler, void *arg);
	void ets_isr_mask(unsigned intr);
	void ets_isr_unmask(unsigned intr);

	void ets_wdt_enable(void);
	void ets_wdt_disable(void);
	void wdt_feed(void);

}

#endif
